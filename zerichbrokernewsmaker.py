﻿
from lxml import html
import requests
import datetime
import urllib, json

class ZerichBrokerNewsMaker:
	def __init__(self):
		self.Url = "https://www.zerich.com/siteServices/getCSRecommendations.html"
		self.BaseUrl = 'https://www.zerich.com'

	def get_news(self):
		result = list()
		response = urllib.urlopen(self.Url)
		news = json.loads(response.read())
		
		direction = {u'0': u'покупать', u'1': u'продавать'}
		
		for item in news:
			current_news = {}
			current_news['title'] = u'Время ' + direction[item['type']] + ' ' + item['issuer']
			
			body = item['content']
			body = body.replace('src="/images', 'src="' + self.BaseUrl + '/images')
			current_news['description'] = body
			current_news['content_type'] = 'html'
			
			raw_pub_dt = item['date_open']
			current_news['url'] = 'https://www.zerich.com/internet-trading/consulting-service.html?id=' + item['id'] + '&dateopen=' + raw_pub_dt
			current_news['author'] = 'zerich'
			
			pub_dt = datetime.datetime.strptime(raw_pub_dt, '%Y-%m-%d')
			current_news['updated'] = pub_dt
			current_news['published'] = pub_dt
			result.append(current_news)
		return result
