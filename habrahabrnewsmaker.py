﻿
import datetime
import feedparser
import re
import threading

class HabrahabrNewsMaker:
	def __init__(self):
		self.Sources = list()
		
	def get_sources(self):
		return self.Sources;

	def get_source_chunks(self, cnt):
		urls = self.get_sources()
		for i in xrange(0, len(urls), cnt):
			yield urls[i:i+cnt]

	def extract_items(self):
		result = {}
		lock = threading.RLock()
		threads = []
		
		count_url_by_thread = 8
	
		for url_chunk in self.get_source_chunks(count_url_by_thread):
			rss_parser = RssParserThread(url_chunk, lock, result)
			rss_parser.start()
			threads.append(rss_parser)

		for thread in threads:
			thread.join()

		return result

	def get_news(self):
		result = list()
		
		items = self.extract_items()
		for key, entry in items.iteritems():
			current_news = {}
			current_news['title'] = entry['title']
			current_news['content_type'] = 'html'
			if 'published' in entry:
				raw_pub_dt = entry['published']
				pub_dt = datetime.datetime.strptime(raw_pub_dt, '%a, %d %b %Y %H:%M:%S %Z')
			else:
				pub_dt = datetime.datetime.now()
			current_news['updated'] = pub_dt
			current_news['published'] = pub_dt
			
			if 'author' in entry:
				entry_author = entry['author']
			else:
				entry_author = 'habrahabr'
			current_news['author'] = entry_author
			
			current_news['description'] = entry['description']
			current_news['url'] = entry['link']
			result.append(current_news)

		return sorted(result, key=lambda i: i['updated'], reverse = True)


		
class RssParserThread(threading.Thread):
	def __init__(self, urls, result_lock, result):
		threading.Thread.__init__(self)
		self.urls = urls
		self.lock = result_lock
		self.news = result

	def is_allow(self, rss_item):
		result = True
		title = rss_item['title'].lower()
		author = rss_item['author'].lower()
		
		for curr_deny_author in self.get_deny_author():			
			if curr_deny_author.lower() == author:
				return False
		
		for curr_title in self.get_allow_title():
			if curr_title.lower() in title:
				return True
		
		bad_titles = self.get_deny_title()
		for curr_title in bad_titles:
			if curr_title.lower() in title:
				return False
		
		for pattern in self.get_deny_title_pattern():
			match = re.search(pattern, title, re.IGNORECASE)
			if match:
				return False;
		
		return result;
	
	def get_deny_author(self):
		result = list()
		result.append(u'alizar')
		result.append(u'SLY_G')
		return result

	def get_allow_title(self):
		result = list()
		result.append(u'oracle')
		result.append(u'teradata')
		result.append(u'dropbox')
		result.append(u'powershell')
		result.append(u'hadoop')
		result.append(u'C#')
		result.append(u'CLR')
		return result

	def get_deny_title_pattern(self):
		result = list()
		result.append(u'node.js')
		result.append(u'React.?OS')
		result.append(u'Kolibri.?OS')
		return result

	def get_deny_title(self):
		result = list()
		result.append(u'Перевод книги «KingPIN»')
		result.append(u'Vim')
		result.append(u'квадрокопт')		
		result.append(u'дрон')
		result.append(u'JBoss')
		result.append(u'Zabrix')
		result.append(u'Fedora')
		result.append(u'Go')
		result.append(u'AutoCAD')
		result.append(u'Yii2')
		result.append(u'Rust')
		result.append(u'Ruby on Rails')
		result.append(u'Tele2')
		result.append(u'c++')
		result.append(u'Telerik')
		result.append(u'InfoboxCloud')
		result.append(u'Nutanix')
		result.append(u'haskell')
		result.append(u'veeam')
		result.append(u'Clojure')
		result.append(u'perl')
		result.append(u'Mac OS')
		result.append(u'iOs')
		result.append(u'LinkedIn')
		result.append(u'Caché')
		result.append(u'Cache')		 
		result.append(u'Voice Application Designer')
		result.append(u'PHP')
		result.append(u'ЦОД')
		result.append(u'Jelastic')
		result.append(u'GCC')
		result.append(u'СХД')
		result.append(u'Sony PlayStation')
		result.append(u'Android SDK')
		result.append(u'Windows Camp')
		result.append(u'Windows Store')
		
		result.append(u'Заберите себе правительство') #цикл публикаций перевода книга Хайлайна
		return result
		 
	def run(self):
		try:
			local_result = {}
			for url in self.urls:
				feed = feedparser.parse(url)
				for item in feed['items']:
					if self.is_allow(item):
						local_result[item['link']] = item
					
			self.lock.acquire()
			for key, item in local_result.iteritems():
				self.news[key] = item
			self.lock.release()
		except Exception as e:
			return None
		else:
			return None
