
from lxml import html
import requests
import datetime

class PiratesruFeedMaker:
	def __init__(self):
		self.Url = "http://www.piratesru.com/feed"

	def get_logo_url(self):
		return "http://open-broker.ru/local/templates/furniture_red/favicon.ico"

	def get_items(self):
		result = list()
		page = requests.get(self.Url)
		tree = html.fromstring(page.text)
		news = tree.xpath('//article[@class="item"]')

		for item in news:
			link = item[0][0]
			current_news = {}
			header = item.text
			url_item =self.BaseUrl + link.attrib['href']
			dt_item = datetime.datetime.strptime(item[1][0].text, '%d.%m.%y')

			current_news['title'] = link.text
			current_news['description'] = u"����� �������� �� ������ ��������� <b><a href='" + url_item + u"'>���</a></b>"
			current_news['content_type'] = 'html'
			current_news['url'] = url_item
			
			current_news['author'] = None
			current_news['updated'] = dt_item
			current_news['published'] = dt_item
			result.append(current_news)
		return result
