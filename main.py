﻿from flask import Flask, request, render_template, url_for
app = Flask(__name__)
app.config['DEBUG'] = True


from urlparse import urljoin
from werkzeug.contrib.atom import AtomFeed

from bankirunewsmaker import BankiruNewsMaker
from habrahabrnewsmaker import HabrahabrNewsMaker
from habrahabrurlbroker import HabrahabrUrlBroker
from OpenBrokerIdeaFeedMaker import OpenBrokerIdeaFeedMaker
from BcsBrokerIdeaFeedMaker import BcsBrokerIdeaFeedMaker
from SmartLabFeedMaker  import SmartLabFeedMaker


@app.route('/')
def site_map():
	links = []
	for rule in app.url_map.iter_rules():
		if "GET" in rule.methods and len(rule.arguments)==0 and rule.endpoint <> 'site_map':
			url = url_for(rule.endpoint)
			links.append((url, rule.endpoint))
	return render_template("site_map.html", links = links)


@app.route('/tripsecrets.atom')
def tripsecrets_feed():
	from tripsecretsnewsmaker import TripsecretsNewsMaker
	atom_name = u'Фильтрованный поток от Tripsecrets.ru'
	
	news_maker = TripsecretsNewsMaker()
	items = news_maker.get_news()
	atom = get_atom(atom_name, request.url, news_maker.Url, items)
	
	return atom.get_response()

@app.route('/piratesru.atom')
def piratesru_feed():
	from piratesrunewsmaker import PiratesruNewsMaker
	atom_name = u'Фильтрованный поток от Pirates.ru'
	
	news_maker = PiratesruNewsMaker()
	items = news_maker.get_news()
	atom = get_atom(atom_name, request.url, news_maker.Url, items)
	
	return atom.get_response()


@app.route('/smartlab2.atom')
@app.route('/smartlabfeedmaker.atom')
def smartlabfeed_feed():
	atom_name = 'SmartLabFeedMaker'
	
	news_maker = SmartLabFeedMaker()
	items = news_maker.get_news()
	atom = get_atom(atom_name, request.url, request.url_root, items)
	
	return atom.get_response()

@app.route('/bankiru.atom')
def bankiru_feed():
	atom_name = 'Bankiru F articles'
	atom_url = request.url
	base_url = request.url_root
	
	bankiru = BankiruNewsMaker()
	items = bankiru.get_news()
	atom = get_atom(atom_name, atom_url, base_url, items)
	
	return atom.get_response()

@app.route('/openbrokeridea.atom')
def openbrokeridea_atom():
	atom_name = u'ИнвестИдеи от открывашки'
	
	news_maker = OpenBrokerIdeaFeedMaker()
	items = news_maker.get_items()
	atom = get_atom(atom_name, request.url, news_maker.Url, items)
	
	return atom.get_response()

@app.route('/bcsbrokerideafeedmaker.atom')
def bcsbrokerideafeedmaker_atom():
	atom_name = u'ИнвестИдеи от БКС'
	
	news_maker = BcsBrokerIdeaFeedMaker()
	items = news_maker.get_items()
	atom = get_atom(atom_name, request.url, news_maker.Url, items)
	
	return atom.get_response()

@app.route('/habrahabr.atom')
def habrahabr_feed():
	atom_name = 'Habrahabr+ F articles'
	
	url_broker = HabrahabrUrlBroker()
	news_maker = HabrahabrNewsMaker()
	news_maker.Sources = url_broker.Sources['habrahabr']
	
	items = news_maker.get_news()
	atom = get_atom(atom_name, request.url, request.url_root, items)
	
	return atom.get_response()

@app.route('/megamozg.atom')
def megamozg_feed():
	atom_name = 'Megamozg+ F articles'
	
	news_maker = HabrahabrNewsMaker()
	url_broker = HabrahabrUrlBroker()
	
	news_maker.Sources = url_broker.Sources['megamozg']
	items = news_maker.get_news()
	atom = get_atom(atom_name, request.url, request.url_root, items)
	
	return atom.get_response()

@app.route('/geektimes.atom')
def geektimes_feed():
	atom_name = 'Geektimes+ F articles'
	
	news_maker = HabrahabrNewsMaker()
	url_broker = HabrahabrUrlBroker()
	
	news_maker.Sources = url_broker.Sources['geektimes']
	items = news_maker.get_news()
	atom = get_atom(atom_name, request.url, request.url_root, items)
	
	return atom.get_response()

@app.route('/zerichbroker.atom')
def zerichbroker_feed():
	from zerichbrokernewsmaker import ZerichBrokerNewsMaker
	atom_name = u'ИнвестИдеи от Zerich'
	
	news_maker = ZerichBrokerNewsMaker()
	items = news_maker.get_news()
	atom = get_atom(atom_name, request.url, news_maker.Url, items)
	
	return atom.get_response()

@app.route('/finamanalyzetechnically.atom')
def FinamAnalyzeTechnically_feed():
	from FinamAnalyzeTechnically import FinamAnalyzeTechnically
	atom_name = u'Финам: анализируем технично'
	
	generator = FinamAnalyzeTechnically()
	items = generator.get_news()
	atom = get_atom(atom_name, request.url, generator.Url, items)
	
	return atom.get_response()
	

def get_atom(name, feed_url, url, content):
	result = AtomFeed(name, feed_url = feed_url, url = url)

	for news in content:
		if 'title_type' in news:		
			title_type_2 = news['title_type']
		else:
			title_type_2 = 'text'


		result.add(news['title'], news['description'],
			title_type = title_type_2,	
			content_type = news['content_type'],
			author = news['author'],
			url = news['url'],
			updated = news['updated'],
			published = news['published'])

	return result


@app.route('/rates.html')
def rates_html():
	from exchangeratenewsmaker import ExchangeRateNewsMaker
	
	news_maker = ExchangeRateNewsMaker()
	rates = news_maker.get_rates()	
	return render_template("rate_diff.html", rates = rates)
	
@app.errorhandler(404)
def page_not_found(e):
	return 'Sorry, nothing at this URL.', 404
