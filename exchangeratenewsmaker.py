﻿
from lxml import html
import requests
import datetime
import urllib, json
import operator

class ExchangeRateNewsMaker:
	def __init__(self):
		self.HomeCurrency = "RUB"	
		self.Url = "http://www.xe.com/currencytables/?from=" + self.HomeCurrency + "&date="
		self.BaseUrl = 'http://www.xe.com'

	def get_exchange_rate(self, dt):
		dt_str = dt.strftime('%Y-%m-%d')		
		url = self.Url + dt_str
		page = requests.get(url)
		tree = html.fromstring(page.text)
		rateTable = tree.xpath('//*[@id="historicalRateTbl"]/tbody/tr')
		rate = {}		
		for rateRow in rateTable:
			currentRate = {}
			currentRate['homeCurrency'] = self.HomeCurrency
			currentRate['currencyCode'] = rateRow[0][0].text
			currentRate['currencyName'] = rateRow[1].text
			currentRate['unitPerHomeCurrency'] = rateRow[2].text
			currentRate['homeCurrencyPerUnit'] = rateRow[3].text
			rate[currentRate['currencyCode']] = currentRate		
		return rate

	def get_rates(self):				
		today = datetime.datetime.now()

		old_rates = self.get_exchange_rate(today - datetime.timedelta(days=7))
		rates = self.get_exchange_rate(today)		

		del rates['XPD']
		for currency_code in rates:
			now_rate = rates[currency_code]
			old_rate = old_rates[currency_code]
				
			old_amt = old_rate['unitPerHomeCurrency']
			now_amt = now_rate['unitPerHomeCurrency']			
			rates[currency_code]['prevUnitPerHomeCurrency'] = old_amt
			rates[currency_code]['prevHomeCurrencyPerUnit'] = old_rate['homeCurrencyPerUnit']			
			rates[currency_code]['ratio'] =  float(now_amt) / float(old_amt)
				
		result = sorted(rates.values(), key = lambda r: -r['ratio']) 
		return result
