﻿import logging
import datetime
from datetime import timedelta
import feedparser
import re

class SmartLabFeedMaker:
	def get_good_person(self):
		result = list()		
		result.append(u'vladimir69')
		result.append(u'Fortress')
		result.append(u'RusBuffet')
		result.append(u'editor2')
		result.append(u'Javelin')
		result.append(u'lSwitchl')
		result.append(u'koblents')
		result.append(u'LaraM')
		result.append(u'malishok')
		
		result.append(u'afanaseva')
		result.append(u'e_albina')
		result.append(u'sfbankir')
		result.append(u'MrWhite')
		result.append(u'klinskih-tag')
		result.append(u'Zdrogov')
		return result

	def get_sources(self):
		result = list()
		result.append(u'http://smart-lab.ru/rss/index/')
		
		for person in self.get_good_person():
			result.append(u'http://smart-lab.ru/rss/personal_blog/' + person + '/blog/personal/')
		return result
		
	def extract_items(self):
		result = {}
		
		urls = self.get_sources()
		for url in urls:
			feed = feedparser.parse(url)
			for item in feed['items']:
				result[item['link']] = item
		return result

	def get_news(self):
		result = list()
		i = 0
		items = self.extract_items()
		for key, entry in items.iteritems():
			i = i+1
			if self.is_allow(entry):
				current_news = {}
				if entry['title'] == "":
					current_news['title'] = "empty title"
				else:
					current_news['title'] = entry['title']
				current_news['content_type'] = 'html'
				if 'published' in entry:
					raw_pub_dt = entry['published']
					pub_dt = datetime.datetime.strptime(raw_pub_dt[:-6], '%a, %d %b %Y %H:%M:%S')
					pub_dt = pub_dt - timedelta(hours = 3) #todo: fix for timezone
				else:
					pub_dt = datetime.datetime.now()
				current_news['updated'] = pub_dt
				current_news['published'] = pub_dt
				
				if 'author' in entry:
					entry_author = entry['author']
				else:
					entry_author = 'habrahabr'
				current_news['author'] = entry_author
				
				current_news['description'] = entry['description']
				current_news['url'] = entry['link']
				result.append(current_news)
		logging.info('count: ' + str(i))
		return sorted(result, key=lambda i: i['updated'], reverse = True)
		
	def is_allow(self, rss_item):
		logging.info(rss_item['link'])


		result = True
		title = rss_item['title'].lower()
		author = rss_item['author'].lower()
		desc = rss_item['description'].lower()

		logging.info("author: " + author)

		for curr_author in self.get_allow_author_by_contains():
			if curr_author.lower() in author:
				logging.info('get_allow_author_by_contains rule')
				return True
		
		for curr_title in self.get_allow_title():
			if curr_title.lower() in title:
				logging.info('get_allow_title rule')
				return True
		
		bad_titles = self.get_deny_title()
		for curr_title in bad_titles:
			if curr_title.lower() in title:
				logging.info('curr_title rule')
				return False
		
		for pattern in self.get_deny_title_pattern():
			match = re.search(pattern, title, re.UNICODE|re.IGNORECASE)
			if match:
				logging.info('get_deny_title_pattern rule')
				return False;

		for curr_author in self.get_deny_author():
			author_info = u'/' + curr_author.lower() + u'/'
			if author_info in author:
				logging.info('get_deny_author rule')
				return False	

		for word in self.get_deny_desc_by_contains():
			if word.lower() in desc:
				logging.info('get_deny_desc_by_contains rule')
				return False
				
		for pattern in self.get_deny_desc_pattern():
			match = re.search(pattern, desc, re.UNICODE|re.IGNORECASE)
			if match:
				logging.info('get_deny_desc_pattern rule')
				return False;		
				
		
		return result;
	
	def get_allow_author_by_contains(self):
		result = list()
		for person in self.get_good_person():
			result.append(u'/' + person + '/')
		return result

	def get_deny_author(self):
		result = list()		
		result.append(u'peretz')		
		result.append(u'gagarin')
		result.append(u'dimonA')
		result.append(u'Kvadr')
		result.append(u'Crazy_Trading')
		result.append(u'zadniy')
		result.append(u'Stalnie')
		result.append(u'venda')
		result.append(u'karpoff1978')
		result.append(u'epanechnikov')
		result.append(u'Derivactiv')
		result.append(u'APanov')
		
		
		result.append(u'sinoptick')
		result.append(u'jelezo')
		
		result.append(u'avtandil')
		result.append(u'trinity')
		result.append(u'pirs557')
		result.append(u'ejik')
		
		result.append(u'alexchi')
		result.append(u'nosmart')
		result.append(u'edvardgrey')
		
		result.append(u'HomeAlone')
		result.append(u'DmI')
		result.append(u'Di-trader')
		
		
		result.append(u'Egorax')
		result.append(u'Alexandr_Gvardiev')
		result.append(u'VacceGanadu')
		result.append(u'AlgoTraderMoexGmailCom')
		result.append(u'Astrolog')
		result.append(u'sspb')
		result.append(u'VacceGanadu')
		result.append(u'YOZHIK')
		result.append(u'STenet')
		result.append(u'Smeshinka')
		result.append(u'nickole')
		result.append(u'kuba')
		result.append(u'Rustem___')
		result.append(u'MDV')
		result.append(u'funti')
		result.append(u'Domingoo')
		result.append(u'amperchick')
		result.append(u'cipia')
		result.append(u'hunter-srike')
		result.append(u'PASHA20procentov')
		result.append(u'bavin2094')
		result.append(u'Lancet')
		result.append(u'Leshai')
		result.append(u'Ae0n')
		result.append(u'ssolok')
		result.append(u'AEA')
		result.append(u'KiboR')
		result.append(u'CosworthRS')
		result.append(u'Speculator2016')
		result.append(u'sortarray')
		result.append(u'gardist')
		result.append(u'Oleg-Bor')
		result.append(u'MarketQuote')
		result.append(u'KibadaFucka')
		result.append(u'TradeByTrade')
		result.append(u'neovuka')
		result.append(u'Scorpio0')
		result.append(u'tree')
		result.append(u'Traderc')		
		result.append(u'ustin101')
		result.append(u'wwxxww')
		result.append(u'Skiv')
		result.append(u'note')
		result.append(u'2153sved')
		result.append(u'alver42')
		result.append(u'manystr')
		result.append(u'FullCup')
		result.append(u'Chapaev')
		result.append(u'PavelDeryabin')
		result.append(u'getstar')
		result.append(u'trader_95')
		result.append(u'Alexandr_KAA')
		result.append(u'andrewuk')
		result.append(u'elliotwaveorg')
		result.append(u'pick')
		result.append(u'Dmitrio')
		result.append(u'bonzamen')
		result.append(u'artemunak')		
		result.append(u'ArtemBond870')
		result.append(u'Tatarin30')
		result.append(u'4KOA')
		result.append(u'stratum')
		result.append(u'forex-light')
		result.append(u'proofpreob')		
		result.append(u'UnitedTraders')
		result.append(u'MaxTrader')
		result.append(u'1jktu')
		result.append(u'Soros1')		
		result.append(u'madeyourtrade')
		result.append(u'ijvan')		
		result.append(u'monkeytim')
		result.append(u'robostock')
		result.append(u'sopernik')
		result.append(u'Albanetz')
		result.append(u'slawa35')
		result.append(u'Maksimov')
		result.append(u'menyaylov23')
		result.append(u'Demonchikkiev')
		result.append(u'amigotrader')
		result.append(u'elf77')
		result.append(u'AnnaPiter')
		result.append(u'Vasilev_Wave')
		result.append(u'NakedTrader')
		result.append(u'Redbull')
		result.append(u'zovrus')
		result.append(u'analitikTA')
		result.append(u'daytrader')
		result.append(u'Video')
		result.append(u'zakk')		
		result.append(u'maxssh')		
		result.append(u'Irina13')
		result.append(u'kochegar')
		result.append(u'sokrjirom')
		result.append(u'slavyanin')
		result.append(u'voskoboy')		
		result.append(u'AnnaF')
		result.append(u'Viacheslav')
		result.append(u'Rita')
		result.append(u'shik555')
		result.append(u'Koyot')
		result.append(u'darthvader')
		result.append(u'plyaskin')
		result.append(u'maikl007')		
		result.append(u'rnosenkocom')
		result.append(u'szander')
		result.append(u'furianez')
		result.append(u'FXFighter')
		result.append(u'LHFgroup')		
		result.append(u'timofeyd')
		result.append(u'NordStream')
		result.append(u'facevalue')
		result.append(u'SPAN_method')
		result.append(u'wwxxww')
		result.append(u'liveandtrade')
		result.append(u'TheMite')
		result.append(u'lomonosov')
		result.append(u'Belochka')
		result.append(u'rtweerer')		
		result.append(u'dhc')
		result.append(u'dani80')
		result.append(u'GuruOfTrading')		
		result.append(u'proton')
		result.append(u'kbrobotru')		
		result.append(u'kalita_finance')
		result.append(u'Collapse')
		result.append(u'QCAP')
		result.append(u'buy_sell')				
		result.append(u'waldhaber')
		result.append(u'VadimTrade')												
		result.append(u'tasha')
		result.append(u'AlgorithmicTrading')
		result.append(u'Slon55')
		result.append(u'JohnnnyPound')
		result.append(u'student1992')
		result.append(u'DimaCab')
		result.append(u'nika8')
		result.append(u'lsd100')
		result.append(u'Metiska1990')
		result.append(u'vodolo1234')
		result.append(u'all_trade')
		result.append(u'zakirov22')		
		result.append(u'rvg')
		result.append(u'PennyStockRU')
		result.append(u'Andrey_Cheba')
		result.append(u'Doctor_Strange')
		result.append(u'Olega')
		result.append(u'neophyte')
		result.append(u'prosper')
		result.append(u'SUNDIO')
		result.append(u'ruscash77')		
		result.append(u'suzdaltsev')
		result.append(u'teacher_of_history')
		result.append(u'mozand23')
		result.append(u'voloxa')
		result.append(u'romario65')		
		return result

	def get_allow_title(self):
		result = list()
		return result

	def get_deny_desc_pattern(self):
		result = list()		
		result.append(u'iframe.*?src.*youtube.com.embed.*')
#		result.append(u'.*пукан.*')
		return result

	def get_deny_desc_by_contains(self):
		result = list()		
		result.append(u'пукан')
		return result
		
	def get_deny_title_pattern(self):
		result = list()
		result.append(u'^[sS]$')
		result.append(u'\sSi\s')
		result.append(u'.*опцион.*конференц.*')
		result.append(u'.*Путь к\s.*')
		result.append(u'.*разгон\s+счет.*')
		result.append(u'.*конф.*смарт.*лаб.*')
		result.append(u'.Здоровье инвестора.')
		result.append(u'(Разогнать|Разгон).*депозит')		
		result.append(u'.*демур.*')
		result.append(u'.*ванют.*')
		result.append(u'.*bitcoin.*')
		result.append(u'.*биткоин.*')
		result.append(u'.*крипт.*')
		result.append(u'.*кречет.*')
		result.append(u'.*скальп.*')
		result.append(u'.*клуб.*анонимных.*трейдеров.*')		
		result.append(u'.*промежуточные.*итоги.*торгов.*')
		result.append(u'.*пут.*к\s+миллион.*')
		result.append(u'.*Si.*итоги\s+дня.*')
		result.append(u'.*мои.*итоги\s+(квартал|дня|год|месяц|недел).*')
		result.append(u'.*Si.*текущий.*момент.*')
		result.append(u'.*философия.*трейд.*')
		result.append(u'запасы\s+нефти')
		result.append(u'по\s+нефти')
		result.append(u'рост\s+нефти')
		result.append(u' лос[ья] ')		
		result.append(u'API\s+')
		result.append(u'чикагс.*бирж')
		result.append(u'.*Золото.+Gella.*')
		result.append(u'.*компания.*связанная.*Алекперов.*Федун.*')
		result.append(u'.*компания.*связанная.*Алекперов.*лукойл.*')
		result.append(u'Трейдинг\s+и\s+здоровье.*')				
		return result
		

	def get_deny_title(self):
		result = list()
		result.append(u'ЛЧИ')
		result.append(u'ЛИЧНЫЙ БЮДЖЕТ')
		result.append(u'Черное и желтое')
		result.append(u'Здоровье трейдера')
		result.append(u'Как я зарабатываю на бирже')
		result.append(u'трансляция торговли')
		result.append(u'технический анализ')
		result.append(u'SensorLive')
		result.append(u'Герчик')
		result.append(u'психо')
		result.append(u'граал')
		result.append(u'сберчат')
		result.append(u'мартынов')
		result.append(u'тимофей')
		result.append(u'Решпект')		
		result.append(u'гусев')
		result.append(u'хомяк')		
		result.append(u'тильт')		
		result.append(u'нефтяные хроники')
		result.append(u'форекс')
		result.append(u'forex')
		result.append(u'Утиные истории')
		result.append(u'Эллиотт')
		result.append(u'срачно')
		result.append(u'еллен') #йеллен		
		result.append(u'Лучшие комментарии прошедшей недели')
		result.append(u'Мой путь к миллион')
		result.append(u'Ситуация на текущий момент')
		result.append(u'Внутридневная торговля фьючерсом сбербанка')
		result.append(u'Недельный торговый план')
		result.append(u'ОПЦИОННЫЙ чат')
		result.append(u'Лудоман')
		result.append(u'мовчан')
		result.append(u'Савченко')
		result.append(u'Левченко')
		result.append(u'Коровин')
		result.append(u'Олейник')
		result.append(u'Маркидонов')
		result.append(u'кукл')
		result.append(u'сиплый')
		result.append(u'Про нефть')
		result.append(u'майни')
		return result

		