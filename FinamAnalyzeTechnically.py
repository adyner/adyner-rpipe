﻿
import datetime
from datetime import timedelta
import feedparser
import re


class FinamAnalyzeTechnically:
	def __init__(self):
		self.Url = "http://www.finam.ru/analysis/nslent/rss.asp"

		patterns = list()
		patterns.append(u'\s*Анализируем\s+технично:\s')
		self.allow_pattern = patterns
		
	def get_logo_url(self):
		return "https://www.finam.ru/favicon.ico"
	

	def is_allow(self, entry):
		descr = entry['description']
		
		for pattern in self.allow_pattern:
			match = re.search(pattern, descr, re.UNICODE|re.IGNORECASE)
			if match:
				return True;
				
		return False;
		
		
	def get_news(self):
		result = list()		
		feed = feedparser.parse(self.Url)
		for entry in feed['items']:
			if self.is_allow(entry):
				current_news = {}
				current_news['content_type'] = 'html'
				current_news['title_type'] = 'html'
				
				if 'published' in entry:
					raw_pub_dt = entry['published']
					pub_dt = datetime.datetime.strptime(raw_pub_dt[:-6], '%a, %d %b %Y %H:%M:%S')
					pub_dt = pub_dt - timedelta(hours = 3) #todo: fix for timezone
				else:
					pub_dt = datetime.datetime.now()
				current_news['updated'] = pub_dt
				current_news['published'] = pub_dt
				
				current_news['author'] = u'Гришин Алексей';
				current_news['title'] = entry['title']
				current_news['description'] = entry['description']
				current_news['url'] = entry['link']
				result.append(current_news)
		return result
