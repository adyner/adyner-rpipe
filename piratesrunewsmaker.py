﻿import datetime
from datetime import timedelta
import feedparser
import re


class PiratesruNewsMaker:
	def __init__(self):
		self.Url = "http://www.piratesru.com/feed"
		self.BaseUrl = 'http://www.piratesru.com'
	
	def get_deny_title_pattern(self):
		result = list()
		result.append(u'.*[Тт][Уу][Рр].*')
		return result
	
	def get_deny_title(self):
		bad_titles = list()
		return bad_titles

	def is_allow(self, entry):
		result = True
		title = entry['title'].lower()
		
		bad_titles = self.get_deny_title()
		for curr_title in bad_titles:
			if curr_title.lower() in title:
				return False
		
		for pattern in self.get_deny_title_pattern():
			match = re.search(pattern, title, re.IGNORECASE)
			if match:
				return False;
				
		return result;		
		
		
	def get_news(self):
		result = list()		
		feed = feedparser.parse(self.Url)
		for entry in feed['items']:
			if self.is_allow(entry):
				current_news = {}
				current_news['content_type'] = 'html'
				
				if 'published' in entry:
					raw_pub_dt = entry['published']
					pub_dt = datetime.datetime.strptime(raw_pub_dt[:-6], '%a, %d %b %Y %H:%M:%S')
					pub_dt = pub_dt - timedelta(hours = 3) #todo: fix for timezone
				else:
					pub_dt = datetime.datetime.now()
				current_news['updated'] = pub_dt
				current_news['published'] = pub_dt
				
				if 'source_name' in entry:
					entry_author = entry['source_name']
				else:
					entry_author = 'banki.ru'
				current_news['author'] = entry_author
				
				current_news['title'] = entry['title']
				current_news['description'] = entry['description']
				current_news['url'] = entry['link']
				result.append(current_news)
		return result
