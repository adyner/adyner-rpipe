﻿
import datetime
from datetime import timedelta
import feedparser
import re


class BankiruNewsMaker:
	def __init__(self):
		self.Url = "http://www.banki.ru/xml/news.rss"
		self.BaseUrl = 'http://www.banki.ru'
	
	def get_deny_title_pattern(self):
		result = list()
		result.append(u'.*бирж.*(АТР|США|Европы).*(закр.*|откр.*)')
		result.append(u'.*бирж.*(закр.*|откр.*).*')
		result.append(u'.*банк.*открыл.нов.*офис.*')
		result.append(u'.*коллектор.*')
		result.append(u'.*(RAEX|moody|S&P).*рейтинг.*')
		result.append(u'.*(RAEX|moody|S&P)\s+(повысил|понизил).*прогноз.*')
		result.append(u'.*цены\s+на\s+нефть\s+(снизил|повыс).*')
		result.append(u'.*Торги.*фонд.*(рынк|бирж).*(Росс|РФ|ММВБ|АТР|США|Европ).*')
		
		return result

	
	def get_deny_title(self):
		bad_titles = list()
		
		bad_titles.append(u'льготное ипотечное кредитование')
		bad_titles.append(u'Стоимость бивалютной корзины на')
		bad_titles.append(u'На аукционе однодневного РЕПО банки взяли')
		bad_titles.append(u'план помощи Греции')
		bad_titles.append(u'В Москве начал работу филиал банка')
		bad_titles.append(u'обновил линейку потребкредитов')
		bad_titles.append(u'Fitch подтвердило рейтинг')
		bad_titles.append(u'S&P подтвердило')
		bad_titles.append(u'НРА подтвердило')
		
		bad_titles.append(u'Фондовые торги в ')
		bad_titles.append(u'Торги на биржах ')
		bad_titles.append(u'Биржи США открылись')
		bad_titles.append(u'торги в США стартовали')
		bad_titles.append(u'Биржи Европы закрылись')
		bad_titles.append(u'биржах европы проходят')
		bad_titles.append(u'Биржи АТР закрылись')
		bad_titles.append(u'Остатки на корсчетах банков в ЦБ составили')
		bad_titles.append(u'Курс доллара и евро на')
		bad_titles.append(u'Средневзвешенный курс доллара на')
		
		return bad_titles

	def is_allow(self, entry):
		result = True
		title = entry['title'].lower()
		
		bad_titles = self.get_deny_title()
		for curr_title in bad_titles:
			if curr_title.lower() in title:
				return False
		
		for pattern in self.get_deny_title_pattern():
			match = re.search(pattern, title, re.IGNORECASE)
			if match:
				return False;
				
		return result;		
		
		
	def get_news(self):
		result = list()		
		feed = feedparser.parse(self.Url)
		for entry in feed['items']:
			if self.is_allow(entry):
				current_news = {}
				current_news['content_type'] = 'html'
				
				if 'published' in entry:
					raw_pub_dt = entry['published']
					pub_dt = datetime.datetime.strptime(raw_pub_dt[:-6], '%a, %d %b %Y %H:%M:%S')
					pub_dt = pub_dt - timedelta(hours = 3) #todo: fix for timezone
				else:
					pub_dt = datetime.datetime.now()
				current_news['updated'] = pub_dt
				current_news['published'] = pub_dt
				
				if 'source_name' in entry:
					entry_author = entry['source_name']
				else:
					entry_author = 'banki.ru'
				current_news['author'] = entry_author
				
				current_news['title'] = entry['title']
				current_news['description'] = entry['description']
				current_news['url'] = entry['link']
				result.append(current_news)
		return result
