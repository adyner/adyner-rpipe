﻿from lxml import html
import requests
import datetime

class BcsBrokerIdeaFeedMaker:
	def __init__(self):
		self.BaseUrl = 'http://bcs-express.ru'
		self.Url = self.BaseUrl + "/investidei"

	def get_items(self):
		result = list()
		page = requests.get(self.Url)
		tree = html.fromstring(page.text)
		news = tree.xpath('//ul[@class="tech_table"]/li[@class="clearfix"]')
		
		for item in news:
			link = item[0][1][0][0][0][0]
			current_news = {}
			header = item.text
			url_item = link.attrib['href']

			current_news['title'] = link.text

			activeItem = item[1][1][0][0][0][0].text
			periodItem = item[2][1][0][0][0].text
			dohodItem = item[3][1][0][0][0][0]

			s = u"Актив: " + activeItem + "<br>"
			s = s + u"Горизонт: " + periodItem + "<br>"
			s = s + u"Ожидаемый доход до " + dohodItem.text + u" годовых<br>"
			s = s + u"Более подробно вы можете прочитать <b><a href='" + url_item + u"'>тут</a></b>"
			current_news['description'] =  s

			current_news['content_type'] = 'html'
			current_news['url'] = url_item
			
			current_news['author'] = None

			dateItem = item[4][1][0][0][0].text
			#current_news['title'] = dateItem
			dt_item = datetime.datetime.strptime(dateItem, '%d.%m.%Y')
			#dt_item = datetime.datetime.now()
			current_news['updated'] = dt_item
			current_news['published'] = dt_item
			result.append(current_news)

		return result
